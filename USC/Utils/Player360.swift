//
//  Player360.swift
//  Rajayoga Meditation
//
//  Created by VRLab on 6/20/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import Foundation
import AVFoundation

class Player360{

    static var instance = Player360()
    private var player:AVPlayer?
    
    
    func PlaywithUrl(url:String){
        let videoURL:URL = Bundle.main.url(forResource: url, withExtension: ".mp4")!
        player = AVPlayer(url:videoURL)
        Play360()
        
    }
    
    func destroyPlayer(){
        player = nil
    }
    
    func getPlayer() -> AVPlayer?{
        return player
        
    }
    func Play360(){
        
        player!.play()
        
    }
    
    func pause360() {
        player!.pause()
    }
    
    
    
}

extension AVPlayer {
    func addProgressObserver(action:@escaping ((Double) -> Void)) -> Any {
        return self.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main, using: { time in
            if let duration = self.currentItem?.duration {
                let duration = CMTimeGetSeconds(duration), time = CMTimeGetSeconds(time)
                let progress = (time/duration)
                action(progress)
            }
        })
    }
}



extension Swifty360View{
    
    
    func set360ImagePlayerView(localImage:UIImage?){
      
        if localImage != nil{
            
            ImageSetup(image: localImage!)
        }
       
    }
    
    private func ImageSetup(image:UIImage){
        
        let motionManager = Swifty360MotionManager.shared
        self.motionStatus = true
        self.setup(image: image, motionManager: motionManager)
        self.setup(image: image, motionManager: motionManager)
    }
    
    func set360VedioPlayerView(videourl:String,player:Player360) {
              //  let videoURL = URL(fileURLWithPath: Bundle.main.path(forResource: "vedio1", ofType: "mp4")
        
        if player.getPlayer() == nil{
            player.PlaywithUrl(url: videourl)
        
        }else{
            let motionManager = Swifty360MotionManager.shared
            self.motionStatus = true
            self.setup(player: player.getPlayer()!, motionManager: motionManager)
        }
    }
    
}



//
//
