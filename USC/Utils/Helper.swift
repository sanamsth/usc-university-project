//
//  Helper.swift
//  Rajayoga Meditation
//
//  Created by sanam on 5/11/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import Foundation


extension UILabel {
    func setHTML(html: String) {
        do {
            
            let attributedString: NSAttributedString = try NSAttributedString(data: html.data(using: .utf8)!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            self.attributedText = attributedString
           
            
        } catch {
            self.text = html
        }
    }
    
    func setFont(iscenter:Bool,font:CGFloat){
        if iscenter{
              self.textAlignment = .center
        }
      
        self.font = UIFont.systemFont(ofSize:font)
    }
}


enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad // iPad style UI
}

struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}
