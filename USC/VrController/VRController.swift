//
//  VRController.swift
//  Rajayoga Meditation
//
//  Created by macMini on 6/8/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit
import AVFoundation

protocol PLayerState {
    func destroyPlayer()
}
class VRController: UIViewController {
    var player:Player360?
    var contentType: ImgVideotype?
    var vedioUrl: String?
    var imageData: String?
    var localImage:UIImage?
    var viewSize:CGFloat = 0
    var delegate:PLayerState?
    @IBOutlet weak var View1: Swifty360View!
    @IBOutlet weak var View2: Swifty360View!
    override func viewDidLoad() {
        super.viewDidLoad()
     // AppUtility.lockOrientation(.landscapeRight)
       
       
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if contentType == .Image360 {
            
            
            
            View1.set360ImagePlayerView(localImage: localImage)
            View2.set360ImagePlayerView(localImage: localImage)
//            self.set360ImagePlayerView(imgUrl: imageData!)
        } else {
            if player == nil && vedioUrl != nil{
                
                player = Player360.instance
                player?.PlaywithUrl(url: self.vedioUrl!)
            }
            
            View1.set360VedioPlayerView(videourl:vedioUrl!, player: player!)
            View2.set360VedioPlayerView(videourl:vedioUrl!, player: player!)
            //self.set360VedioPlayerView(videourl: vedioUrl!)
        }
    }
   

    @IBAction func backButton(_ sender: Any) {
       
              delegate?.destroyPlayer()
             AppUtility.lockOrientation(.portrait)
            self.dismiss(animated: false, completion: nil)
        
        
    }
   
    

}
