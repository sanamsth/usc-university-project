//
//  CollectionCell360List.swift
//  Rajayoga Meditation
//
//  Created by macMini on 6/9/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit

class CollectionCell360List: UICollectionViewCell,ResuableIdentifier,ReusableView {
   
    
    @IBOutlet weak var imageview: CustomImageView!
    
    @IBOutlet weak var placeHolder: UIImageView!
    
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // tableview.registerCell(type: VrVideosCell.self)
         //tableview.isScrollEnabled = false
        imageview.layer.cornerRadius = 10
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true;
    
    }
    
    
   
    
    func ConfigureCell(data:HomeData){
        self.title.text = data.title
        self.subtitle.text = data.subtitle
        self.imageview.image = UIImage(named: data.thumbnail)
        placeHolder.image = UIImage(named: "360image")
       
    }
    
    func ConfigureCell(data:EventsData){
        
        self.title.text = data.title
        self.subtitle.text = data.subtitle
        self.imageview.image = UIImage(named: data.thumbnail)
        placeHolder.image = UIImage()
    }
    
    func setBoldTitle(){
        self.title.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
    }
    
}
