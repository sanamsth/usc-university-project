//
//  Second360Cell.swift
//  Rajayoga Meditation
//
//  Created by macMini on 6/8/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit

protocol CollectionViewDelegate {
    func itemClicked(index:HomeData)

}

class Second360Cell: UITableViewCell,ReusableView,ResuableIdentifier,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
   private var imageData:[HomeData]?

    @IBOutlet weak var collectionView: UICollectionView!

    var delegate:CollectionViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         collectionView.registerCell(type: CollectionCell360List.self)
        collectionView.delegate = self
        collectionView.dataSource = self

        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true;
       
    }
    
    func ConfigureCell(data:[HomeData]){
        
        self.imageData = data
        collectionView.reloadData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if imageData?.count == 0{
            return 0
        }else{
            return imageData!.count
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:CollectionCell360List = collectionView.dequeCell(indexpath: indexPath)
    
        cell.ConfigureCell(data: imageData![indexPath.row])
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
              return CGSize(width: collectionView.frame.width/2.4, height: collectionView.frame.height)
            
        case .phone:
               return CGSize(width: collectionView.frame.width-30, height: collectionView.frame.height)
        default:
              return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
        }
     
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.itemClicked(index: imageData![indexPath.row])
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        
//        return 20
//    }
    
    
}
