//
//  First360ViewCell.swift
//  Rajayoga Meditation
//
//  Created by macMini on 6/8/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit



class First360ViewCell: UITableViewCell,ResuableIdentifier,ReusableView {
  
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var vrbutton: UIButton!
    @IBOutlet weak var compasView: Swifty360CompassView!
    @IBOutlet weak var swiftyView: Swifty360View!
    
    var image360:UIImage?
    var homecontroller:HomeViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        swiftyView.layer.cornerRadius = 10
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true;
        setupSwiftyView(image: UIColor.darkGray.image())
    // indicator.startAnimating()
      //  vrbutton.isUserInteractionEnabled = false
        
    }
    
    

    
    func setupSwiftyView(image:UIImage?){
        
        self.swiftyView.set360ImagePlayerView(localImage: image)
        self.swiftyView.compassDelegate = self.compasView
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.reorientVerticalCameraAngle))
        self.swiftyView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    
    func ConfigCell(data:HomeData){
         self.image360 = UIImage(named: data.thumbnail)
        setupSwiftyView(image: UIImage(named: data.thumbnail))
      
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func vrTapped(_ sender: Any) {
       
        print("vr item clicked")
        homecontroller?.vrContentType = .Image360
        homecontroller?.img = self.image360
        homecontroller!.presentVrController()
        
    }
    @objc func reorientVerticalCameraAngle() {
            swiftyView.reorientVerticalCameraAngleToHorizon(animated: true)
        }
    
}


extension UIColor {
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
