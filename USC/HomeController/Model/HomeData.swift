//
//  HomeData.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation


struct HomeData:Codable{
    let title:String
    let subtitle:String
    let filename:String
    let thumbnail:String
    let type:String

    
   static func getCampusData() ->[HomeData]?{
        
        let url = Bundle.main.url(forResource: "Campusdata", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        
        let decoder = JSONDecoder()
        let homedata = try! decoder.decode([HomeData].self, from: data)
        
        return homedata
    }
    
    static func getVrData() -> [HomeData]?{
        let url = Bundle.main.url(forResource: "vrdata", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        
        let vrdata = try! JSONDecoder().decode([HomeData].self, from: data)
        
        return vrdata
    }
    
    
    
}
