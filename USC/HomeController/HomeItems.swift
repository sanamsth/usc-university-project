//
//  HomeItems.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

enum HomeItems {
    case HeaderSection
    case ImageSection
    case VRSection
}

protocol Home360Items {
    var homeitems:HomeItems {get}
    var rowcount:Int {get}
    var title:String {get}
    var sectionTitle:String {get}
}
