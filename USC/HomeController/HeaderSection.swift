//
//  HeaderSection.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

class HeaderSection: Home360Items {
    var homeitems: HomeItems{
        
        return .HeaderSection
    }
    
    var rowcount: Int
    
    var title: String
    
    var sectionTitle: String
    
    var homeheaderData:[HomeData]
    init(homeheaderdata:[HomeData]) {
        
        self.homeheaderData = homeheaderdata
        self.title = ""
        self.rowcount = homeheaderData.count
        self.sectionTitle = ""
    }
    
    
}
