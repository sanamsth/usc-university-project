//
//  HomeViewController.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit

enum ImgVideotype{
    case Image360
    case Video360
}

class HomeViewController: UIViewController,Revealable,CollectionViewDelegate,PLayerState {
    
    
    var img:UIImage?
    private var vrurl:String?
    var menuButton: UIBarButtonItem!
    let dataSource = HomeDataController()
    var vrContentType:ImgVideotype?
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupRevealMenu(controller: self)
        tableview.registerCell(type: First360ViewCell.self)
        tableview.registerCell(type: Second360Cell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        
    }
    
    func itemClicked(index: HomeData) {
        print("item clicked")
        
        let vc = ControllerDataSource.instance.get360ViewerController()
        vc.modalPresentationStyle = .fullScreen
        vc.data360 = index
        if index.type == "image"{
            vc.IVtype = .Image360
        }else{
            vc.IVtype = .Video360
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentVrController(){
        let vc = ControllerDataSource.instance.getVRViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.view.backgroundColor = .black
        vc.contentType = vrContentType
        vc.player = nil
        vc.localImage = img
        vc.delegate = self
        vc.vedioUrl = self.vrurl
        AppUtility.lockOrientation(.landscapeRight)
        self.present(vc, animated: false, completion: nil)
    }
    
    func destroyPlayer() {
        
    }
    
    
}


extension HomeViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataSource.items.count != 0{
            return dataSource.items.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if dataSource.items.count != 0{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = dataSource.items[indexPath.section]
        
        switch data.homeitems {
        case .HeaderSection:
            let cell:First360ViewCell = tableView.dequeCell(indexpath: indexPath)
            if let data = self.Get360Data(type:data.homeitems,section:indexPath.section){
                
                cell.homecontroller = self
                cell.selectionStyle = .none
                cell.ConfigCell(data: data.first!)
                
            }
            return cell
        case .ImageSection:
            
            let cell:Second360Cell = tableView.dequeCell(indexpath: indexPath)
            if let data = self.Get360Data(type:data.homeitems,section:indexPath.section){
                cell.delegate = self
                cell.selectionStyle = .none
                cell.ConfigureCell(data: data)
                
            }
            return cell
            
            
        case .VRSection:
            
            let cell:Second360Cell = tableView.dequeCell(indexpath: indexPath)
            if let data = self.Get360Data(type:data.homeitems,section:indexPath.section){
                cell.delegate = self
                cell.ConfigureCell(data: data)
                
            }
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            
            return self.view.frame.height/2
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            return  self.view.frame.height/3.9
        }else{
            return  self.view.frame.height/3.4
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section != 0{
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            let title = UILabel(frame: view.bounds)
            title.text = dataSource.items[section].sectionTitle
            title.font = UIFont.boldSystemFont(ofSize: 18)
            
            view.addSubview(title)
            
            return view
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return dataSource.items[section].sectionTitle
    }
    
    func Get360Data(type:HomeItems,section:Int)-> [HomeData]? {
        
        switch type {
            
            
        case .HeaderSection:
            let data = (dataSource.items[section] as! HeaderSection).homeheaderData
            return data.reversed()
            
        case .ImageSection:
            let data = (dataSource.items[section] as! Image360Section).homeimagedata
            return data
            
        case .VRSection:
            let data = (dataSource.items[section] as! VRSection).vrdata
            return data.reversed()
        }
        
    }
    
    
    
}
