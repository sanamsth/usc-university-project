//
//  HomeDataController.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation


class HomeDataController{
    
    var items:[Home360Items] = [Home360Items]()
    
    
    init()
    {
        get360Images()
        getVRVideos()
    }
    
    
    func get360Images(){
        let data = HomeData.getCampusData()
        let headersection = HeaderSection(homeheaderdata: data!)
        
        self.items.append(headersection)
        
        let imgsection = Image360Section(homeimgdata: data!)
        self.items.append(imgsection)
        
        
    }
    
    func getVRVideos(){
        
        let data = HomeData.getVrData()
        let vrsection = VRSection(vrdata: data!)
        
        self.items.append(vrsection)
        
        
    }
    
}
