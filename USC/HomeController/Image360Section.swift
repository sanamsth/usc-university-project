//
//  Image360Section.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

class Image360Section: Home360Items {
    var homeitems: HomeItems{
        return .ImageSection
    }
    
    var rowcount: Int
    
    var title: String
    
    var sectionTitle: String
    
    var homeimagedata:[HomeData]
    
    init(homeimgdata:[HomeData]) {
        self.homeimagedata = homeimgdata
        self.title = ""
        self.sectionTitle = "Image 360"
        self.rowcount = homeimagedata.count
        
    }
}
