//
//  VrVideosSection.swift
//  USC
//
//  Created by sanam on 7/2/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

class VRSection:Home360Items{
    var homeitems: HomeItems{
        
        return .VRSection
    }
    
    var rowcount: Int
    
    var title: String
    
    var sectionTitle: String
    
    var vrdata:[HomeData]
    
    init(vrdata:[HomeData]) {
        self.vrdata = vrdata
        self.title = ""
        self.sectionTitle = "VR Videos"
        self.rowcount = vrdata.count
    }
    
    
}
