//
//  Router.swift
//  USC
//
//  Created by sanam on 7/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import UIKit

struct Router {
    
    let title:String
    let SbName:String
    let details:String
    
    func AddController() -> UIViewController {
        
        let sb = UIStoryboard(name: SbName, bundle: nil)
        
        guard let controller = sb.instantiateInitialViewController() else {
            fatalError()
        }
        controller.title = title
        return controller
        
    }
}


struct ControllerDataSource {
    
    static let instance = ControllerDataSource()
    
    private init(){
        
    }
    
    func getRoomNavigationController() -> RoomNavigationController{
        
        return Router(title: "WAYFINDING", SbName: "RoomNavigationSb", details: "").AddController() as! RoomNavigationController
    }
    
    func getHomeViewController() -> HomeViewController{
        
        return Router(title: "Home", SbName: "Home360Sb", details: "").AddController() as! HomeViewController
    }
    
    func getVRViewController() -> VRController{
        return Router(title: "", SbName: "VRSB", details: "").AddController() as! VRController
    }
    
    func getRevealViewController() -> SWRevealViewController{
        
        return Router(title: "", SbName: "Main", details: "").AddController() as! SWRevealViewController
    }
    
    func get360ViewerController() -> Viewer360Controller{
        return Router(title: "", SbName: "Viewer360", details: "").AddController() as! Viewer360Controller
       }
    
    func getRearViewControllerList() -> [UIViewController]{
        
        let viewControllers = [getHomeViewController(),getRoomNavigationController(),getEventsController(),getGalleryController(),getAboutUsController()]
        
        return viewControllers
        
    }
    
    func getAboutUsController() -> MoreViewController{
        
        return Router(title: "About Us", SbName: "More", details: "").AddController() as! MoreViewController
    }
    
    func getGalleryController() -> InstagramController{
        
        return Router(title: "Gallery", SbName: "InstagramSB", details: "").AddController() as! InstagramController
    }
    
    func getEventsController() -> EventsViewController{
        
        return Router(title: "", SbName: "EventsSb", details: "").AddController() as! EventsViewController
    }
    
    func getEventsDetailsController() -> EventsDetailViewController{
          
          return Router(title: "", SbName: "EventsDetailsSb", details: "").AddController() as! EventsDetailViewController
      }
}
