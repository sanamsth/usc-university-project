//
//  EventsData.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

struct EventsData:Codable{
    let title:String
    let subtitle:String
    let filename:String
    let thumbnail:String
    let type:String
//    let news:String

    
   static func getCampusEventsData() ->[EventsData]?{
        
        let url = Bundle.main.url(forResource: "Campusdata", withExtension: "json")
        let data = try! Data(contentsOf: url!)
        
        let decoder = JSONDecoder()
        let homedata = try! decoder.decode([EventsData].self, from: data)
        
        return homedata
    }
    
//    static func getVrData() -> [HomeData]?{
//        let url = Bundle.main.url(forResource: "vrdata", withExtension: "json")
//        let data = try! Data(contentsOf: url!)
//
//        let vrdata = try! JSONDecoder().decode([HomeData].self, from: data)
//
//        return vrdata
//    }
//
    
    
}
