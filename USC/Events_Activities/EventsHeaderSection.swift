//
//  EventsHeaderSection.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation


class EventsHeaderSection:USCEvents{
    
    var eventsitems: EventsItems{
        return .HeaderSection
    }
    
    var rowcount: Int
    var title: String
    
    var sectionTitle: String
    var evenstadata:[EventsData]
    
    init(eventsData:[EventsData]) {
        self.evenstadata = eventsData
        self.title = ""
        self.rowcount = 1
        self.sectionTitle = "Latest Events"
    }
    
    
}


class EventsNewsListSection:USCEvents{
    var eventsitems: EventsItems{
        
        return .NewsListSection
    }
    
    var rowcount: Int
    
    var title: String
    
    var sectionTitle: String
    var evenstadata:[EventsData]
    
    init(eventsData:[EventsData]) {
        self.evenstadata = eventsData
        self.title = ""
        self.rowcount = evenstadata.count
        self.sectionTitle = "Hot News"
    }
    
    
}
