//
//  EventsDataController.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation


class EventsDataController{
    
    var items:[USCEvents] = [USCEvents]()
    
    
    init()
    {
        getHeaderNews()
        getNewList()
    }
    
    
    func getHeaderNews(){
        let data = EventsData.getCampusEventsData()
        let headersection = EventsHeaderSection(eventsData: data!)
        
        self.items.append(headersection)
        
//        let imgsection = Image360Section(homeimgdata: data!)
//        self.items.append(imgsection)
//
        
    }
    
    func getNewList(){
        
        let data = EventsData.getCampusEventsData()
        let newslistsection = EventsNewsListSection(eventsData: data!)
        
        self.items.append(newslistsection)
        
    }
    
}
