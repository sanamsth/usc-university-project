//
//  NewsListTableViewCell.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit

class NewsListTableViewCell: UITableViewCell,ReusableView,ResuableIdentifier {

    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageview.layer.cornerRadius = 5
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func ConfigureCell(data:EventsData){
        
//        self.title.text = data.title
//        self.subtitle.text = data.subtitle
        self.imageview.image = UIImage(named: data.thumbnail)
        
        
    }
}
