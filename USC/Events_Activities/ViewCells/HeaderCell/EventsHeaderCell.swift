//
//  EventsHeaderCell.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit

protocol EventsCollectionCellDelegate {
    func didselectCell(data:EventsData)
}

class EventsHeaderCell: UITableViewCell,ReusableView,ResuableIdentifier,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
  private var eventsdata:[EventsData]?

    
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate:EventsCollectionCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.registerCell(type: CollectionCell360List.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func CongifureCell(data:[EventsData]){
        self.eventsdata = data
          collectionView.reloadData()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if eventsdata?.count == 0{
                   return 0
               }else{
                   return eventsdata!.count
               }
             
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
           let cell:CollectionCell360List = collectionView.dequeCell(indexpath: indexPath)
           
               cell.ConfigureCell(data: eventsdata![indexPath.row])
        cell.setBoldTitle()
               return cell
          
      }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
              return CGSize(width: collectionView.frame.width/2.4, height: collectionView.frame.height)
            
        case .phone:
               return CGSize(width: collectionView.frame.width-30, height: collectionView.frame.height)
        default:
              return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
        }
        
     
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate!.didselectCell(data:eventsdata![indexPath.row])
         
         
     }

    
    
}
