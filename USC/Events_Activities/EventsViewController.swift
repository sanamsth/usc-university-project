//
//  EventsViewController.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController,Revealable {
    var menuButton: UIBarButtonItem!
    
    
    @IBOutlet weak var tableview: UITableView!
    let datasource:EventsDataController = EventsDataController()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.presentTransparentNavigationBar()
        self.setupRevealMenu(controller: self)
        tableview.registerCell(type: EventsHeaderCell.self)
        tableview.registerCell(type: NewsListTableViewCell.self)
        // Do any additional setup after loading the view.
        
    }
    
}


extension EventsViewController:UITableViewDelegate,UITableViewDataSource,EventsCollectionCellDelegate{
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return datasource.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return datasource.items[section].rowcount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = datasource.items[indexPath.section]
        
        switch data.eventsitems {
        case .HeaderSection:
            let cell:EventsHeaderCell = tableView.dequeCell(indexpath: indexPath)
            if let data = self.Get360Data(type:data.eventsitems,section:indexPath.section){
                cell.delegate = self
                cell.selectionStyle = .none
                cell.CongifureCell(data: data)
                
            }
            return cell
        case .NewsListSection:
            let cell:NewsListTableViewCell = tableView.dequeCell(indexpath: indexPath)
            if let data = self.Get360Data(type:data.eventsitems,section:indexPath.section){
                
                cell.selectionStyle = .none
                cell.ConfigureCell(data: data[indexPath.row])
                
            }
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            
            return self.view.frame.height/3.5
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            return  self.view.frame.height/3.9
        }else{
            return  self.view.frame.height/8
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let title = UILabel(frame: view.bounds)
        title.text = datasource.items[section].sectionTitle
        title.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        
        view.addSubview(title)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return datasource.items[section].sectionTitle
    }
    
    func Get360Data(type:EventsItems,section:Int)-> [EventsData]? {
        
        switch type {
            
            
        case .HeaderSection:
            let data = (datasource.items[section] as! EventsHeaderSection).evenstadata
            return data
            
        case .NewsListSection:
            let data = (datasource.items[section] as! EventsNewsListSection).evenstadata
            return data.reversed()
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = ControllerDataSource.instance.getEventsDetailsController()
        vc.modalPresentationStyle = .fullScreen
        let data = datasource.items[indexPath.section]
        let itemsdata = Get360Data(type: data.eventsitems, section: indexPath.section)![indexPath.row]
        vc.image = UIImage(named: itemsdata.thumbnail)
        present(vc, animated: true, completion: nil)
        
        
    }
    
    func didselectCell(data: EventsData) {
        let vc = ControllerDataSource.instance.getEventsDetailsController()
        vc.modalPresentationStyle = .fullScreen
        
        let itemsdata = data
        vc.image = UIImage(named: itemsdata.thumbnail)
        present(vc, animated: true, completion: nil)
    }
    
}
