//
//  EventsItems.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

enum EventsItems {
    case HeaderSection
    case NewsListSection
}

protocol USCEvents {
    var eventsitems:EventsItems {get}
    var rowcount:Int {get}
    var title:String {get}
    var sectionTitle:String {get}
}
