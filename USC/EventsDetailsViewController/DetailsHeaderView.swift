//
//  DetailsHeaderView.swift
//  USC
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import UIKit

protocol BackButtonClicked {
    func DismissViewController()
}

class DetailsHeaderView:UIView{
    
    var delegate:BackButtonClicked?
    @IBOutlet weak var imageview:UIImageView!
    
    @IBAction func backbutton(_ sender: Any) {
        
        delegate?.DismissViewController()
        
    }
    
    
    
    
}
