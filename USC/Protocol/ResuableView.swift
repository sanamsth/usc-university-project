
//
//  ResuableView.swift
//  Example
//
//  Created by VRLab on 3/22/19.
//  Copyright © 2019 Moayad Al kouz. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView {
    
}

extension ReusableView where Self:UIView{
    
   static var nibName:String{
        
        return String(describing: self)
    }
}
