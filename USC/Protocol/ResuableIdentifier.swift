//
//  ResuableIdentifier.swift
//  Example
//
//  Created by VRLab on 3/22/19.
//  Copyright © 2019 Moayad Al kouz. All rights reserved.
//

import Foundation
import UIKit

protocol ResuableIdentifier {
    
    
}

extension ResuableIdentifier where Self:UIView{
    
   static var reuseIdentifier:String{
        
        return String(describing: self)
    }
}


