//
//  Viewer360Controller.swift
//  Rajayoga Meditation
//
//  Created by VRLab on 6/20/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit
import AVFoundation


class Viewer360Controller: UIViewController {

    @IBOutlet weak var rajayogaTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var vrbutton: UIButton!
    
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var compassView: Swifty360CompassView!
    var player:Player360 = Player360.instance
    @IBOutlet weak var swiftyView: Swifty360View!
    var data360:HomeData?
    var IVtype:ImgVideotype?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    AppUtility.lockOrientation(.portrait)
        // Do any additional setup after loading the view.
       rajayogaTitle.text = data360?.title
        date.text = data360?.subtitle
       progressIndicator.startAnimating()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.reorientVerticalCameraAngle))
        self.swiftyView.addGestureRecognizer(tapGestureRecognizer)
        vrbutton.isUserInteractionEnabled = false
    }
    
   
    @objc func reorientVerticalCameraAngle() {
        if IVtype == .Video360{
        if playPauseButton.isHidden{
            
            playPauseButton.isHidden = false
        }else{
             playPauseButton.isHidden = true
        }
        }
        swiftyView.reorientVerticalCameraAngleToHorizon(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
            if object as AnyObject? === player.getPlayer() {
                if keyPath == "timeControlStatus" {
                    if player.getPlayer()!.timeControlStatus == .playing {
                        DispatchQueue.main.async {
                            self.progressIndicator.stopAnimating()
                                                   self.progressIndicator.isHidden = true
                                                  self.playPauseButton.isHidden = false
                                                   self.playPauseButton.setImage(UIImage(named: "pausebutton"), for: .normal)
                        }
                      
                    }else{
                          DispatchQueue.main.async {
                          self.playPauseButton.setImage(UIImage(named: "playbutton"), for: .normal)
                        }
                        
                    }
                }
            
        }
          
        }
    
    
    @IBAction func playVideos(_ sender: Any) {
        
       PausePlay()
    }
    
    func PausePlay(){
        if player.getPlayer()!.timeControlStatus == .playing{
            
            player.pause360()
        }else{
            player.Play360()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            if self.IVtype == .Image360{
                self.playPauseButton.isHidden = true
                
                
                    self.vrbutton.isUserInteractionEnabled = true
                self.swiftyView.set360ImagePlayerView(localImage: UIImage(named: self.data360!.thumbnail))
                    self.swiftyView.compassDelegate = self.compassView
                    self.progressIndicator.stopAnimating()
                    self.progressIndicator.isHidden = true
                    

                
               
            }else{
                
                 self.playPauseButton.isHidden = false
                self.set360VedioPlayerView(videoUrl: self.data360!.filename)
                self.swiftyView.compassDelegate = self.compassView
                self.vrbutton.isUserInteractionEnabled = true
                //  player.Play360()
                
            }
        }
    
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        player.getPlayer()?.removeObserver(self, forKeyPath: "timeControlStatus")
        player.destroyPlayer()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func VrClicked(_ sender: Any) {
        
       
        let vc = ControllerDataSource.instance.getVRViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.contentType = IVtype
        vc.player = player
        vc.imageData = data360?.thumbnail
        vc.localImage = UIImage(named: data360!.thumbnail)
        vc.vedioUrl = data360?.filename
        AppUtility.lockOrientation(.landscapeRight)
        self.present(vc, animated: false, completion: nil)
        
    }
    
    func set360VedioPlayerView(videoUrl:String) {
        if player.getPlayer() == nil{
            self.player.PlaywithUrl(url: videoUrl)
            let motionManager = Swifty360MotionManager.shared
            self.swiftyView.motionStatus = true
            self.swiftyView.setup(player: self.player.getPlayer()!, motionManager: motionManager)
             self.player.getPlayer()?.addObserver(self, forKeyPath: "timeControlStatus", options: [.new,.old], context: nil)
            
        }
        
     
        
        
     
    }
}
