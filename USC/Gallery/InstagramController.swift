//
//  InstagramController.swift
//  Rajayoga Meditation
//
//  Created by macMini on 5/26/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit
import DTPhotoViewerController
import Gemini
class InstagramController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,Revealable,UICollectionViewDelegateFlowLayout {
    var menuButton: UIBarButtonItem!
   
    
    @IBOutlet weak var stackview: NSLayoutConstraint!
    @IBOutlet weak var collectionView: GeminiCollectionView!
    var imagedata = ["sc1","sc2","sc3","sc4","sc5","sc6"]
    fileprivate var selectedImageIndex: Int = 0
    var viewController:SimplePhotoViewerController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackview.constant = 40
     
       // self.showHUD(progressLabel:"")
        self.setupRevealMenu(controller: self)
        collectionView.registerCell(type: InstagramViewCell.self)

       
//        InstagramClient.getInstaPhotos { (data, error) in
//
//            self.imagedata = data?.data?.filter{$0.type == "image"}
//            self.collectionView.reloadData()
//            self.dismissHUD(isAnimated: true)
//        }
        
        collectionView.gemini
            .scaleAnimation()
            .scale(0.87)
            .scaleEffect(.scaleUp)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func InstagramHandler(_ sender: Any) {
        //InstagramClient.openInstagram(instagramHandle: "rajayogameditation")
        openInstagram(instagramHandle:"uofsc")
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
    self.collectionView.animateVisibleCells()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            return imagedata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:InstagramViewCell = collectionView.dequeCell(indexpath: indexPath)
      //  cell.contentView.setCardView(view: cell.contentView)
       
        cell.ConfigureCell(data: imagedata[indexPath.row])
         self.collectionView.animateCell(cell)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? InstagramViewCell {
            self.collectionView.animateCell(cell)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            return CGSize(width: collectionView.frame.width-100, height: collectionView.frame.height)
        case .phone:
            return CGSize(width: collectionView.frame.width-80, height: collectionView.frame.height)
        default:
            print("other")
        }
        
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.height-10)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedImageIndex = indexPath.row
        if let cell = collectionView.cellForItem(at: indexPath) as? InstagramViewCell {
            cell.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
           
            self.viewController = SimplePhotoViewerController(referencedView: cell.imageview, image: cell.imageview.image)
            viewController!.dataSource = self
            viewController!.delegate = self
         
            self.present(viewController!, animated: true, completion: nil)
            

        }
    }
     func openInstagram(instagramHandle: String) {
          guard let url = URL(string: "https://instagram.com/\(instagramHandle)")  else { return }
          if UIApplication.shared.canOpenURL(url) {
              if #available(iOS 10.0, *) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
              } else {
                  UIApplication.shared.openURL(url)
              }
          }
      }

}


extension InstagramController:DTPhotoViewerControllerDataSource{
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = self.collectionView?.cellForItem(at: indexPath) as? InstagramViewCell {
            return cell.imageview
        }
        
        return nil
    }
    
//    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
//        // Set text for each item
//        if let cell = cell as? CustomPhotoCollectionViewCell {
//            cell.extraLabel.text = "Image no \(index + 1)"
//        }
//    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return imagedata.count
    }
    
  
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
     
             imageView.image = UIImage(named:imagedata[index])
       
    }
}

extension InstagramController: SimplePhotoViewerControllerDelegate {
    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
                
                let image = UIImage(named:imagedata[index])
                
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
           
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Error occured", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.viewController!.present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Image saved.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
           self.viewController!.present(ac, animated: true)
        }
    }
    
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: selectedImageIndex, section: 0)
            
            // If cell for selected index path is not visible
            if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    
}

