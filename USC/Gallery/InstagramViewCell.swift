//
//  InstagramViewCell.swift
//  Rajayoga Meditation
//
//  Created by VRLab on 5/27/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import UIKit
import Gemini
class InstagramViewCell: GeminiCell,ResuableIdentifier,ReusableView {

    @IBOutlet weak var imageview: CustomImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageview.layer.cornerRadius = 24

    }
    
    
    func ConfigureCell(data:String){
        
        self.imageview.image = UIImage(named: data)
        
//        imageview.loadImageUsingUrlString(urlString:(data.images?.standard_resolution!.url!)!)
        
//        ImageHandler.getImage(url: URL(string: (data.images?.standard_resolution!.url)!)!) { (image, error) in
//            
//            self.imageview.image = image
//        }
        
    }

}
