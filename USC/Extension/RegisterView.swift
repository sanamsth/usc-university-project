//
//  RegisterView.swift
//  Example
//
//  Created by VRLab on 3/22/19.
//  Copyright © 2019 Moayad Al kouz. All rights reserved.
//

import Foundation
import UIKit
extension UITableView{
    
    func registerCell<T:UITableViewCell>(type:T.Type) where T:ReusableView,T:ResuableIdentifier{
        
        let nib = UINib(nibName: type.nibName, bundle: nil)
        
        register(nib, forCellReuseIdentifier: type.reuseIdentifier)
        
    }
    
    
    
    func dequeCell<T:UITableViewCell>(indexpath:IndexPath) -> T where T:ResuableIdentifier{
        
        guard let cell = dequeueReusableCell(withIdentifier:T.reuseIdentifier , for: indexpath) as? T else{
            fatalError("cannot deque")
        }
        
        return cell
    }
}


extension UICollectionView{
    
    func registerCell<T:UICollectionViewCell>(type:T.Type) where T:ReusableView,T:ResuableIdentifier{
        
        let nib = UINib(nibName: type.nibName, bundle: nil)
        
       register(nib, forCellWithReuseIdentifier: type.reuseIdentifier)
        
    }
    
    
    
    func dequeCell<T:UICollectionViewCell>(indexpath:IndexPath) -> T where T:ResuableIdentifier{
        
        guard let cell = dequeueReusableCell(withReuseIdentifier:T.reuseIdentifier , for: indexpath) as? T else{
            fatalError("cannot deque")
        }
        
        return cell
    }
    
}
