//
//  ImageService.swift
//  TMDB
//
//  Created by VRLab on 3/19/19.
//  Copyright © 2019 VRLab. All rights reserved.
//

import Foundation
import UIKit

class ImageHandler{
    
    static let cache = NSCache<NSString,UIImage>()
    
    
    private static func downloadImage(url:URL,completion:@escaping(UIImage?,Error?) -> Void){
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            var downloadedImage:UIImage? = UIImage()
            if let data = data{
                
                downloadedImage = UIImage(data: data)
                if downloadedImage != nil{
                    cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
                    
                    DispatchQueue.main.async {
                        completion(downloadedImage,nil)
                    }
                }
                
            }else{
                completion(nil,error)
            }
            
            }.resume()
        
    }
    
    
    static func getImage(url:URL,completion:@escaping(UIImage?,Error?) -> Void){
        
        if let image = cache.object(forKey: url.absoluteString as NSString){
            completion(image,nil)
            
        }else{
            downloadImage(url: url, completion: completion)
        }
        
        
    }
}


extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) 
        self.layer.masksToBounds = true
    }
}


let imageCache = NSCache<AnyObject, AnyObject>()
class CustomImageView: UIImageView {
    var imageUrlString:String?
    func loadImageUsingUrlString(urlString:String) {
        imageUrlString = urlString
        let url = URL(string:urlString)
        image = nil
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
            
        }
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if error != nil {
                    print("\(error)")
                    return
                    
            }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data:data!)
                    if self.imageUrlString == urlString
                {
                    self.image = imageToCache
                  
                    }
                    if imageToCache != nil{
                          imageCache.setObject(imageToCache!, forKey:urlString as AnyObject)
                    }
                 
                    
            }
            
        }
            ).resume()
        
}
}
