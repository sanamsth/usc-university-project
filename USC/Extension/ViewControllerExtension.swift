//
//  ViewControllerExtension.swift
//  Rajayoga Meditation
//
//  Created by VRLab on 4/25/19.
//  Copyright © 2019 KtmStudio. All rights reserved.
//

import Foundation

extension UIViewController {
    func setupRevealMenu<T : UIViewController>(controller : T) where T : Revealable {
        
        if self.revealViewController() != nil{
            
            controller.menuButton = UIBarButtonItem(image: UIImage(named: "menu_icon"), style: .plain, target: nil, action: nil)
           // controller.menuButton.tintColor = UIColor(red: 255/255, green: 147/255, blue: 0, alpha: 1)
            navigationItem.leftBarButtonItem = controller.menuButton
            controller.menuButton.target = self.revealViewController()
            controller.menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(controller.revealViewController().panGestureRecognizer())
            // self.revealViewController().rearViewRevealWidth = 200
        }
    }
}



protocol Revealable:class {
    var menuButton: UIBarButtonItem! { get set }
}

extension Notification.Name {
    static let dataDownloadCompleted = Notification.Name(
        rawValue: "dataDownloadCompleted")
    
    static let scrolledtoBottom = Notification.Name(
     rawValue: "scrollToBottom"
    )
    static let scrolledtoTop = Notification.Name(
        rawValue: "scrollToTop"
    )
}
