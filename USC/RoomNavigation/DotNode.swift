//
//  DotNode.swift
//  FLowers
//
//  Created by sanam on 6/28/20.
//  Copyright © 2020 Paracosma. All rights reserved.
//

import Foundation
import ARKit

class DotNode:SCNNode{
    
    
    override init() {
        super.init()
        circlenode()
//        CreateCustomTriangle()
    }
    
    init(position:SCNVector3) {
        super.init()
       
        circlenode()
//        CreateCustomTriangle()
         self.position = position
              self.rotation = SCNVector4(x: 1,
                y: 0,
                z: 0,
                w: -Float.pi / 2)
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func CreateCustomTriangle(){
        
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: 0.0, y: 0.3))
        path.addLine(to: CGPoint(x: 0.2, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: 0.1))
        path.addLine(to: CGPoint(x: -0.2, y: 0.0))
        path.close()
        
        let shape = SCNShape(path: path, extrusionDepth: 0.01)
        shape.firstMaterial?.diffuse.contents = UIColor.yellow
        shape.chamferRadius = 0.01
        
//        let node = SCNNode(geometry: shape)
//        node.position.z = -1
//      node.rotation = SCNVector4(x: 1,
//        y: 0,
//        z: 0,
//        w: -Float.pi / 2)
        
              self.geometry = shape
        
//              let omniLight = SCNLight()
//               omniLight.type = .ambient
//               omniLight.color = UIColor.yellow
//               self.light = omniLight
               
//               self.filters = addBloom()
        
        
    }
    
    func circlenode(){
        
        let sphere = SCNSphere(radius: 0.03)
        sphere.firstMaterial?.diffuse.contents = UIColor.red
        self.geometry = sphere
        
        let omniLight = SCNLight()
        omniLight.type = .ambient
        omniLight.color = UIColor.yellow
        self.light = omniLight
        
        self.filters = addBloom()
        
        
    }
    
    func addBloom() -> [CIFilter]? {
        let bloomFilter = CIFilter(name:"CIBloom")!
        bloomFilter.setValue(10.0, forKey: "inputIntensity")
        bloomFilter.setValue(50.0, forKey: "inputRadius")

        return [bloomFilter]
    }
    
    
}
