//
//  MapsData.swift
//  ARPersistence
//
//  Created by sanam on 6/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


struct MapsData:Codable {
    let name:String
    let mapData:Data
    
    init(name:String,mapData:Data) {
        self.name = name
        self.mapData = mapData
    }
}

class UserRepository {
    enum Key: String, CaseIterable {
        case name, avatarData
        func make(for userID: String) -> String {
            print(" name ",self.rawValue + "_" + userID )
            return self.rawValue + "_" + userID
        }
    }
    let userDefaults: UserDefaults
    // MARK: - Lifecycle
    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }
    
    func SaveData(mapsData:MapsData){
        
        if var data =  getData(){
            
            data.append(mapsData)
            storeData(data: data)
            
        }else{
            
            var iData = Array<MapsData>()
            
            iData.append(mapsData)
            
            storeData(data: iData)
            
        }
    }
    
    
    private func storeData(data:[MapsData]){
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data){
            UserDefaults.standard.set(encoded, forKey: "maps")
        }
        
    }
    
    func getData() -> [MapsData]?{
        
        if let data = userDefaults.data(forKey: "maps"){
        let decoder = JSONDecoder()
        
        let mapdata = try? decoder.decode([MapsData].self, from: data)
        
            return mapdata
        }
        return nil
    }
    
    func removedata(filename:String){
        
        guard let data = getData() else {
            fatalError("no data")
        }
        
        let modifieddata = data.filter{$0.name != filename}
        storeData(data: modifieddata)
        
    }
    
    
    // MARK: - API
    func storeInfo(forUserID userID: String, name: String, avatarData: Data) {
        saveValue(forKey: .name, value: name, userID: userID)
        saveValue(forKey: .avatarData, value: avatarData, userID: userID)
    }
    
    func getUserInfo(forUserID userID: String) -> (name: String?, avatarData: Data?) {
        let name: String? = readValue(forKey: .name, userID: userID)
        let avatarData: Data? = readValue(forKey: .avatarData, userID: userID)
        return (name, avatarData)
    }
    
    func removeUserInfo(forUserID userID: String) {
        Key
            .allCases
            .map { $0.make(for: userID) }
            .forEach { key in
                userDefaults.removeObject(forKey: key)
        }
    }
    // MARK: - Private
    private func saveValue(forKey key: Key, value: Any, userID: String) {
        userDefaults.set(value, forKey: key.make(for: userID))
    }
    private func readValue<T>(forKey key: Key, userID: String) -> T? {
        return userDefaults.value(forKey: key.make(for: userID)) as? T
    }
}
