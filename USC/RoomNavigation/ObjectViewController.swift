//
//  ObjectViewController.swift
//  ARPersistence
//
//  Created by VRLab on 6/30/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol MapDataSeletedDelegate {
    func getMapData(mapdata:MapsData)
}

class ObjectViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   

    @IBOutlet weak var tableVIew: UITableView!
    var mapdata:[MapsData]?
    var delegate:MapDataSeletedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        preferredContentSize = CGSize(width: 250, height: tableVIew.contentSize.height)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        
        return mapdata?.count ?? 0
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        tablecell.textLabel?.text = mapdata![indexPath.row].name
        
        return tablecell
        
       }
       
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.getMapData(mapdata: mapdata![indexPath.row])
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
