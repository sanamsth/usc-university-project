//
//  CartViewController.swift
//  Store App
//
//  Created by Bikash on 5/24/18.
//  Copyright © 2018 prayogshaala. All rights reserved.
//

import UIKit
import SafariServices

class MoreViewController: UIViewController,Revealable {
    var menuButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.setupRevealMenu(controller: self)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func websiteTapped(_ sender: Any) {
        let sf = SFSafariViewController(url: URL(string: "https://www.sc.edu")!)
         sf.modalPresentationStyle = .fullScreen
        self.present(sf, animated: true, completion: nil)
    }
    
    
}
